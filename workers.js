// Use of Cloudflare KV Required. Read the Docs on https://gitlab.com/GoogleDriveIndex/Shared-Drives-List before deploying.

const clientId = '746239575955-c4d2o1ahg4ts6ahm3a5lh5lp9g8m15h4.apps.googleusercontent.com';
const clientSecret = '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';
const refreshToken = '';
const service_account = false
const auth_on = false
const auth = [{
        user: "one",
        pass: "two"
    },
    {
        user: "two",
        pass: "four"
    },
];
const service_account_json = {}
var authConfig = {}
const html = `<!DOCTYPE html>
<html>
   <head>
      <title>Shared Drive List</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootswatch@4/dist/flatly/bootstrap.min.css">
   </head>
   <body>
      <header>
         <nav class="navbar navbar-dark bg-dark navbar-expand-lg">
            <a href="/" class="navbar-brand d-flex align-items-center">
            <img src="https://images.cdn.hashhackers.com/logo/logo-white-d.svg" class="btn1" width="220" height="40">
            </a>
            <div class="collapse navbar-collapse" id="navbarNav">
               <ul class="navbar-nav ">
               </ul>
            </div>
         </nav>
      </header>
      <div class="container mt-5 mb-5">
         <ul id="drive-list" class="list-group">
            <li class="list-group-item active text-center">Shared Drives List</li>
            <!-- JavaScript code will populate the rest of the list -->
         </ul>
         <div class="mt-5 d-flex justify-content-center">
            <div id="loading-bar" class="spinner-border" role="status">
               <span class="sr-only">Loading...</span>
            </div>
         </div>
         <div id="drive-count" class="text-center mt-3"></div>
      </div>
      <!-- Bootstrap JavaScript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.3/umd/popper.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
      <script>
         let nextPageToken = null; // Initialize nextPageToken as null
         let driveCount = 0; // Initialize driveCount as 0
         let isLoading = false; // Initialize isLoading flag as false
         
         $(document).ready(() => {
         // Function to fetch JSON data and populate the list
         const fetchData = (token) => {
         const requestUrl = '/api.json';
         
         // Create the request body as JSON data
         const requestBody = JSON.stringify({ nextPageToken: token });
         
         // Show the loading bar if it's not already shown and not loading
         if (!$('#loading-bar').is(':visible') && !isLoading) {
            $('#loading-bar').show();
         }
         
         isLoading = true; // Set isLoading flag to true
         
         fetch(requestUrl, { method: 'POST', body: requestBody })
            .then(response => response.json())
            .then(data => {
         	  // Loop through the drives array in the JSON response
         	  for (const drive of data.drives) {
         		 const listItem = $('<li class="text-wrap">').addClass('list-group-item d-flex justify-content-between align-items-center');
         		 const nameSpan = $('<span class="d-inline-block text-break">').text(drive.name).appendTo(listItem);
         
         		 // Make the list item clickable to open the corresponding Google Drive folder
         		 listItem.click(() => {
         			const folderUrl = "https://drive.google.com/drive/folders/" + drive.id;
         			window.open(folderUrl, '_blank');
         		 });
         
         		 $('#drive-list').append(listItem);
         		 driveCount++; // Increment driveCount
         	  }
         
         	  nextPageToken = data.nextPageToken; // Update nextPageToken
         
         	  if (nextPageToken) {
         		 // If nextPageToken exists, continue loading more data
         		 $(window).on('scroll', loadMoreDataOnScroll); // Attach scroll event listener
         	  } else {
         		 // If nextPageToken doesn't exist, hide the "Load More" button
         		 $('#load-more-btn').hide();
         
         		 // Hide the loading bar if there's no ongoing fetch request
         		 $('#loading-bar').hide();
         	  }
         
         	  updateDriveCount(); // Update the drive count
         
         	  isLoading = false; // Set isLoading flag to false
            })
            .catch(error => {
         	  console.error('Error:', error);
         	  isLoading = false; // Set isLoading flag to false in case of an error
            });
         };
         
         // Function to update the drive count
         const updateDriveCount = () => {
         $('#drive-count').text(driveCount + " Drives Loaded");
         };
         
         // Function to load more data when scrolling to the bottom of the page
         const loadMoreDataOnScroll = () => {
         // Check if the user has scrolled to the bottom of the page
         if ($(window).scrollTop() + $(window).height() === $(document).height()) {
            // Check if there is a next page token and not currently loading
            if (nextPageToken && !isLoading) {
         	  $(window).off('scroll', loadMoreDataOnScroll); // Remove scroll event listener temporarily
         	  fetchData(nextPageToken);
            }
         }
         };
         
         // Fetch data initially
         fetchData(nextPageToken);
         });
         
            
      </script>
   </body>
</html>
`
async function handleRequest(request) {
    const authHeader = request.headers.get('Authorization');
    // Check if the Authorization header exists and contains the expected credentials
    if (auth_on) {
        if (!authHeader || !isAuthenticated(authHeader)) {
            // Authentication failed, return a 401 Unauthorized response
            return new Response('Unauthorized', {
                status: 401,
                headers: {
                    'WWW-Authenticate': 'Basic realm="Restricted Area"',
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                },
            });
        }
    }
    let requrl = new URL(request.url);
    let path = requrl.pathname;
    if (path == '/') {
        // Handle the root path
        return new Response(html, {
            status: 200,
            headers: {
                "content-type": "text/html;",
                "Access-Control-Allow-Origin": "*",
            },
        });
    } else if (path == '/api.json' && request.method === 'POST') {
        // Handle the '/api.json' path
        try {
            const posted_Data = await request.json(); // Extract the JSON body from the request
            // Handle the POST data as needed
            const bearer_token = await accessToken();
            // Build the API request URL
            const url = 'https://www.googleapis.com/drive/v3/drives';
            const params = new URLSearchParams({
                pageSize: '100',
                pageToken: posted_Data.nextPageToken || '',
            });
            // Send the API request to check if the billing account is active
            const response = await fetch(url + "?" + params, {
                headers: {
                    Authorization: `Bearer ${bearer_token}`,
                },
            });
            var text = await response.text();
            if (response.ok) {
                return new Response(text, {
                    status: 200,
                    headers: {
                        "content-type": "application/json;",
                        "Access-Control-Allow-Origin": "*",
                    },
                });
            } else {
                return new Response(JSON.stringify(text), {
                    status: 500,
                    headers: {
                        "content-type": "application/json;",
                        "Access-Control-Allow-Origin": "*",
                    },
                });
            }
        } catch (error) {
            return new Response('Error processing POST data', {
                status: 500
            });
        }
    } else {
        // Handle other paths
        return new Response('404', {
            status: 404,
            headers: {
                "content-type": "application/json;",
                "Access-Control-Allow-Origin": "*",
            },
        });
    }
}

function isAuthenticated(authHeader) {
    const credentials = authHeader.split(' ')[1];
    const decodedCredentials = atob(credentials);
    const [username, password] = decodedCredentials.split(':');
    // Check if the provided credentials match any entry in the auth array
    return auth.some(entry => entry.user === username && entry.pass === password);
}
async function accessToken() {
    if (authConfig.accessToken) {
        console.log('Using cached token');
        return authConfig.accessToken;
    }
    var refresh_token_expiry = await ENV.get("expiry");
    if (refresh_token_expiry == void 0 || refresh_token_expiry <= Date.now()) {
        console.log("Generating New Token")
        const obj = await fetchAccessToken();
        //console.log("Refresh Token: " + obj.access_token)
        var fullDate = new Date(Date.now()).toLocaleString();
        if (obj.access_token != void 0) {
            authConfig.accessToken = obj.access_token;
            authConfig.expires = Date.now() + 3500 * 1e3;
        }
        await ENV.put("refresh_token", authConfig.accessToken);
        await ENV.put("expiry", authConfig.expires);
        return authConfig.accessToken;
    } else {
        console.log("Using old Token")
        authConfig.accessToken = await ENV.get("refresh_token");
        //console.log("Got Saved Refresh Token: " + this.authConfig.accessToken)
        return authConfig.accessToken;
    }
    console.log("Something Went Wrong with Refresh Token: " + authConfig.accessToken)
    return authConfig.accessToken;
}
async function fetchAccessToken() {
    const url = "https://www.googleapis.com/oauth2/v4/token";
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };
    var post_data;
    if (service_account) {
        console.log('Service Account')
        const jwttoken = await JSONWebToken.generateGCPToken(service_account_json);
        post_data = {
            grant_type: 'urn:ietf:params:oauth:grant-type:jwt-bearer',
            assertion: jwttoken,
        };
    } else {
        console.log('Refresh Token')
        post_data = {
            client_id: clientId,
            client_secret: clientSecret,
            refresh_token: refreshToken,
            grant_type: "refresh_token",
        };
    }
    let requestOption = {
        'method': 'POST',
        'headers': headers,
        'body': await enQuery(post_data)
    };
    let response;
    for (let i = 0; i < 3; i++) {
        response = await fetch(url, requestOption);
        if (response.ok) {
            break;
        }
        await this.sleep(800 * (i + 1));
    }
    return await response.json();
}
const JSONWebToken = {
    header: {
        alg: 'RS256',
        typ: 'JWT'
    },
    importKey: async function(pemKey) {
        var pemDER = this.textUtils.base64ToArrayBuffer(pemKey.split('\n').map(s => s.trim()).filter(l => l.length && !l.startsWith('---')).join(''));
        return crypto.subtle.importKey('pkcs8', pemDER, {
            name: 'RSASSA-PKCS1-v1_5',
            hash: 'SHA-256'
        }, false, ['sign']);
    },
    createSignature: async function(text, key) {
        const textBuffer = this.textUtils.stringToArrayBuffer(text);
        return crypto.subtle.sign('RSASSA-PKCS1-v1_5', key, textBuffer)
    },
    generateGCPToken: async function(serviceAccount) {
        const iat = parseInt(Date.now() / 1000);
        var payload = {
            "iss": serviceAccount.client_email,
            "scope": "https://www.googleapis.com/auth/drive",
            "aud": "https://oauth2.googleapis.com/token",
            "exp": iat + 3600,
            "iat": iat
        };
        const encPayload = btoa(JSON.stringify(payload));
        const encHeader = btoa(JSON.stringify(this.header));
        var key = await this.importKey(serviceAccount.private_key);
        var signed = await this.createSignature(encHeader + "." + encPayload, key);
        return encHeader + "." + encPayload + "." + this.textUtils.arrayBufferToBase64(signed).replace(/\//g, '_').replace(/\+/g, '-');
    },
    textUtils: {
        base64ToArrayBuffer: function(base64) {
            var binary_string = atob(base64);
            var len = binary_string.length;
            var bytes = new Uint8Array(len);
            for (var i = 0; i < len; i++) {
                bytes[i] = binary_string.charCodeAt(i);
            }
            return bytes.buffer;
        },
        stringToArrayBuffer: function(str) {
            var len = str.length;
            var bytes = new Uint8Array(len);
            for (var i = 0; i < len; i++) {
                bytes[i] = str.charCodeAt(i);
            }
            return bytes.buffer;
        },
        arrayBufferToBase64: function(buffer) {
            let binary = '';
            let bytes = new Uint8Array(buffer);
            let len = bytes.byteLength;
            for (let i = 0; i < len; i++) {
                binary += String.fromCharCode(bytes[i]);
            }
            return btoa(binary);
        }
    }
};

function enQuery(data) {
    const ret = [];
    for (let d in data) {
        ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
    }
    return ret.join('&');
}

function sleep(ms) {
    return new Promise(function(resolve, reject) {
        let i = 0;
        setTimeout(function() {
            //console.log('sleep' + ms);
            i++;
            if (i >= 2) reject(new Error('i>=2'));
            else resolve(i);
        }, ms);
    })
}
addEventListener('fetch', event => {
    event.respondWith(handleRequest(event.request))
})
